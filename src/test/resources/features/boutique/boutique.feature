@boutique
Feature: Boutique Listing

  Scenario: Show all boutique pages on the homepage
    Given User on the boutique listing page
    When Show all boutique pages

  Scenario: Go to all boutique pages and calculate performance
    Given User on the boutique listing page
    When Show all boutique pages
    When Request to all boutique pages