@login
Feature: Login

  Background:
    Given User on the homepage
    When User opens the Login Popup

  Scenario Outline: Login - Positive Scenarios
    And User try to login with following credentials:
      | Email   | Password   |
      | <Email> | <Password> |
    Then User should successfully logged-in

    Examples:
      | Email                    | Password   |
      | ttechtest@mailinator.com | Qazxsw123. |

########################################################################################################################

  Scenario Outline: Login - Negative Scenarios
    And User try to login with following credentials:
      | Email   | Password   |
      | <Email> | <Password> |
    Then User should see error message as "<Error Text>"

    Examples:
      | Email                     | Password   | Error Text                              |
      |                           |            | Lütfen email adresinizi giriniz.        |
      |                           | Qazxsw123. | Lütfen email adresinizi giriniz.        |
      | ttechtest@mailinator.com  |            | Lütfen şifre giriniz.                   |
      | ttechtest@mailinator.com  |            | Lütfen şifre giriniz.                   |
      | ttechtest@mailinator      | Qazxsw123. | Lütfen email adresinizi giriniz.        |
      | ttechtest@mailinator      | Qaz        | Lütfen email adresinizi giriniz.        |
      | ttechtest@ mailinator.com | Qazxsw123  | Lütfen email adresinizi giriniz.        |
      | tttest@mailinator.com     | Qazxsw123. | Hatalı E-Posta / Şifre. Tekrar Deneyin. |
      | ttechtest@mailinator.com  | Qazxsw     | Hatalı E-Posta / Şifre. Tekrar Deneyin. |
