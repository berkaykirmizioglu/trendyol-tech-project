@bookapi
Feature: Books Feature

  Scenario: Check empty book store
    When API User "GET" request to "/api/books/" with followings:
      | Type   | Key          | Value            |
      | Header | Content-type | application/json |
    Then API User see service response status code should be "200"
    Then API User see response contains string "message" as "There are no books in store"

########################################################################################################################

  Scenario: Verify that the id field is read-only
    When API User "PUT" request to "/api/books/" with followings:
      | Type   | Key          | Value                                                   |
      | Header | Content-type | application/json                                        |
      | Body   |              | [{"id": 1, "author": "John Smith", "title": "SRE 101"}] |
    Then API User see service response status code should be "400"
    Then API User see response contains string "errorText" as "id is read-only, you cannot send it in request body!"

########################################################################################################################

  Scenario: Put a book into the Book Store
    When API User "PUT" request to "/api/books/" with followings:
      | Type   | Key          | Value                                          |
      | Header | Content-type | application/json                               |
      | Body   |              | [{"author": "John Smith", "title": "SRE 101"}] |
    Then API User see service response status code should be "200"
    Then API User see response contains string "id" as "1"
    Then API User see response contains string "author" as "John Smith"
    Then API User see response contains string "title" as "SRE 101"

########################################################################################################################

  Scenario: Get a book into the Book Store
    And API User "GET" request to "/api/books/1" with followings:
      | Type   | Key          | Value            |
      | Header | Content-type | application/json |
    Then API User see service response status code should be "200"
    Then API User see response contains string "id" as "1"
    Then API User see response contains string "author" as "John Smith"
    Then API User see response contains string "title" as "SRE 101"

########################################################################################################################

  Scenario: Should not put a book same author and title
    When API User "PUT" request to "/api/books/" with followings:
      | Type   | Key          | Value                                          |
      | Header | Content-type | application/json                               |
      | Body   |              | [{"author": "John Smith", "title": "SRE 101"}] |
    Then API User see service response status code should be "400"
    Then API User see response contains string "errorText" as "Another book with similar title and author already exists."

########################################################################################################################

  Scenario Outline: Check /api/books mandatory fields
    When API User "PUT" request to "/api/books/" with followings:
      | Type   | Key          | Value                                        |
      | Header | Content-type | application/json                             |
      | Body   |              | [{"author": "<author>", "title": "<title>"}] |
    Then API User see response contains string "errorText" as "<errorText>"

    Examples:
      | author     | title   | errorText                 |
      |            |         | author and title required |
      | Alan Smith |         | title is required         |
      |            | SRE 102 | author is required        |

########################################################################################################################



