package bdd.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;

import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class WireMockManager {

    public static final int WIREMOCK_PORT_NUMBER = 8090;
    public static final int WIREMOCK_SECURE_PORT_NUMBER = 8043;
    public static final String WIREMOCK_HOST = "localhost";
    private static com.github.tomakehurst.wiremock.WireMockServer wireMockServer = null;


    private WireMockManager() {

    }
    private static WireMockServer getInstance() throws InterruptedException {
        if (wireMockServer == null) {
            wireMockServer = new com.github.tomakehurst.wiremock.WireMockServer(wireMockConfig()
                    .httpsPort(WIREMOCK_SECURE_PORT_NUMBER).port(WIREMOCK_PORT_NUMBER));
            Thread.sleep(2000);
            configureFor(WIREMOCK_HOST, WIREMOCK_PORT_NUMBER);
        }

        return wireMockServer;
    }

    public static void start() throws InterruptedException {

        getInstance().start();
    }

    public static void stop() throws InterruptedException {
        getInstance().stop();
    }
}
