package bdd.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;

public class Stubs {



    public static void createStubs() throws InterruptedException {

        WireMockManager.start();
        stubFor(get(urlEqualTo("/api/books/")).inScenario("There are no books in server")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("SECOND")
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("[ \"message\": \"There are no books in store!\"]")));

        stubFor(put(urlEqualTo("/api/books/")).inScenario("Put a book via API")
                .withRequestBody(equalToJson("{}"))
                .whenScenarioStateIs("SECOND")
                .willSetStateTo("THIRD")
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("[{\"author\": \"John Smith\", \"title\": \"SRE 101\"}]")));

        stubFor(put(urlEqualTo("/api/books/")).inScenario("Cannot put a book via API")
                .whenScenarioStateIs("THIRD")
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(400)
                        .withBody("{ \"error\": \"Another book with similar title and author already exists.\" }")));

        stubFor(get(urlEqualTo("/api/books/([1-9]*)/")).inScenario("There are some books")
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("[ {\"id\":1, \"author\": \"John Smith\", \"title\": \"SRE 101\"}]")));

    }
}
