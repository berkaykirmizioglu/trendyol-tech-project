package bdd.stepdefinitions.bookapi;

import bdd.requester.SimpleRequester;
import bdd.wiremock.Stubs;
import bdd.wiremock.WireMockManager;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.model.TestFailureException;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;


public class BookAPIStepDefinitions {

    ValidatableResponse response;

    @Before
    public void before() throws InterruptedException {
        Stubs.createStubs();
    }

    @After
    public void after() throws InterruptedException {
        WireMockManager.stop();
    }

    @Steps
    SimpleRequester simpleRequester;

    @When("^API User \"([^\"]*)\" request to \"([^\"]*)\" with followings:$")
    public void genericRequestToBookApi(String method, String endpoint, List<Map<String, String>> parameterList) throws TestFailureException, InterruptedException {

        response = simpleRequester.genericRequest(method, endpoint, parameterList);
    }

    @Then("^API User see response contains string \"([^\"]*)\" as \"([^\"]*)\"$")
    public void shouldKeyContainsValue(String key, String value) {

        if (!key.equals("") || (!value.equals(""))) {
            MatcherAssert.assertThat(response.extract().path(key), containsString(value));
        }
    }

    @Then("^API User see service response status code should be \"([^\"]*)\"")
    public void shouldSeeResponseStatusCodeAs(int statusCode) throws Exception {

        if (response == null) {
            throw new Exception("Request should be completed before assertion!");
        }

        MatcherAssert.assertThat(response.extract().statusCode(), Matchers.equalTo(statusCode));
    }
}
