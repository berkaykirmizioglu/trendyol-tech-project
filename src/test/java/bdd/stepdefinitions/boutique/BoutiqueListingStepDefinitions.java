package bdd.stepdefinitions.boutique;

import bdd.steps.boutique.BoutiqueListingSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class BoutiqueListingStepDefinitions {

    BoutiqueListingSteps boutiqueListingSteps;

    @Given("^User on the boutique listing page$")
    public void userOnTheBoutiqueListingPage() {

        boutiqueListingSteps.openBoutiqueListingPage();
    }

    @When("^Show all boutique pages$")
    public void showAllBoutiquePages() {

        boutiqueListingSteps.showAllBoutiquesWithScroll();
    }

    @When("^Request to all boutique pages$")
    public void requestToAllBoutiquePages() {

        boutiqueListingSteps.showAllBoutiquesWithScroll()
                            .requestToAllBoutiquePages();
    }
}
