package bdd.stepdefinitions;

import bdd.steps.HomePageSteps;
import bdd.steps.boutique.BoutiqueListingSteps;
import bdd.utils.javascript.JsCommon;
import com.gargoylesoftware.htmlunit.util.XmlUtils;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;
import java.util.Map;

public class HomePageStepDefinitions {

    HomePageSteps homePageSteps;

    @Given("^User on the homepage$")
    public void userOnTheHomepage() {

        homePageSteps.openHomepage();
    }

    @When("^User opens the Login Popup$")
    public void userOpensTheLoginPopup() {
        homePageSteps.openLoginPopup();
    }

    @And("^User try to login with following credentials:$")
    public void userTryToLogin(List<Map<String, String>> credentials) {

        String email = credentials.get(0).get("Email");
        String password = credentials.get(0).get("Password");

        homePageSteps.typeEmail(email)
                     .typePassword(password)
                     .submitLoginForm();
    }

    @Then("^User should successfully logged-in$")
    public void userShouldLoggedIn() {
        homePageSteps.isLoggedIn();
    }

    @Then("^User should see error message as \"([^\"]*)\"$")
    public void userShouldSeeErrorMessageAs(String errorText) {
        homePageSteps.userShouldSeeErrorMessage(errorText);
    }
}
