package bdd.requester;

import bdd.statics.ApiStatics;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.model.TestFailureException;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.when;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class SimpleRequester {

    @Step("SimpleRequester to {0}")
    public static ValidatableResponse get(String url) {

        ValidatableResponse response = when().request("GET", url).then();

        return response;
    }

    @Step("Generic BookAPI Request")
    public ValidatableResponse genericRequest(String method, String endpoint, List<Map<String, String>> parameters) throws TestFailureException, InterruptedException {

        ValidatableResponse response = null;
        RequestSpecification requestSpecification = SerenityRest.given();

        for (Map<String, String> parameter : parameters) {
            if (parameter.get("Type").equals("Header")) {
                requestSpecification.header(parameter.get("Key"), parameter.get("Value"));
            } else if (parameter.get("Type").equals("QueryParam")) {
                requestSpecification.queryParam(parameter.get("Key"), parameter.get("Value"));
            }
            else if (parameter.get("Type").equals("Body")) {
                requestSpecification.body(parameter.get("Value"));
            } else {
                throw new TestFailureException("Check your table syntax!");
            }
            switch (method) {
                case "GET":
                    response = requestSpecification.when().get(ApiStatics.BOOK_API_BASE_URI + endpoint).then();
                    break;
                case "PUT":
                    response = requestSpecification.when().put(ApiStatics.BOOK_API_BASE_URI + endpoint).then();
                    break;
                case "POST":
                    response = requestSpecification.when().post(ApiStatics.BOOK_API_BASE_URI + endpoint).then();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + method);
            }
        }

        assertNotNull(response);
        assertNotEquals(String.format("%s HTTP 500 Internal Server Error.", endpoint),500,
                                                response.extract().statusCode());
        return response;
    }
}