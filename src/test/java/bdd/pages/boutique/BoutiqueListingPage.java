package bdd.pages.boutique;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("page:trendyol.boutique.listing.page")
public class BoutiqueListingPage extends PageObject {

    public static final By BOUTIQUE_DETAIL_WIDGET = By.xpath("//article[@class='component-item']//a");
}