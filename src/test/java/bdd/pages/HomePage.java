package bdd.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("page:trendyol.home.page")
public class HomePage extends PageObject {

    public static final By ACCOUNT_BUTTON = By.id("accountBtn");
    public static final By EMAIL_FIELD = By.id("email");
    public static final By PASSWORD_FIELD = By.id("password");
    public static final By LOGIN_BUTTON = By.id("loginSubmit");
    public static final By ERROR_BOX = By.id("errorBox");
    public static final By ACCOUNT_LOGIN_BUTTON = By.xpath("//div[@class='account-button login']");
    public static final By MY_ACCOUNT_BUTTON = By.id("logged-in-container");
}