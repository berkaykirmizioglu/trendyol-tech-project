package bdd.steps.boutique;

import bdd.pages.boutique.BoutiqueListingPage;
import bdd.requester.SimpleRequester;
import bdd.utils.csv.CsvUtils;
import bdd.utils.javascript.Scroll;
import bdd.utils.wait.PageWaits;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BoutiqueListingSteps extends UIInteractionSteps {

    BoutiqueListingPage boutiqueListingPage;

    List<WebElement> boutiques;

    @Step("Open boutique listing page.")
    public void openBoutiqueListingPage() {

        boutiqueListingPage.open();
    }

    @Step("Show all boutiques and calculate loading performance after scroll.")
    public BoutiqueListingSteps showAllBoutiquesWithScroll() {

        List<String[]> performanceResults= new ArrayList<>();
        new PageWaits().waitForPageToCompleteState(getDriver());
        while (true) {
            boutiques = getDriver().findElements(BoutiqueListingPage.BOUTIQUE_DETAIL_WIDGET);
            String currentUrlBeforeScroll = getDriver().getCurrentUrl();

            String loadTime = Scroll.toLastElement(getDriver(), boutiques);
            performanceResults.add(new String[]{loadTime});


            waitABit(2000);
            String currentUrlAfterScroll = getDriver().getCurrentUrl();

            if (currentUrlBeforeScroll.equals(currentUrlAfterScroll)) {

                System.out.println("You are in end of the page!");
                break;
            }
        }
        CsvUtils.writeDataLineByLine("src/test/resources/csv/scrollPerformance.csv",
                new String[]{"LoadTime"}, performanceResults);

        return this;

    }

    @Step("SimpleRequester to all boutique pages and check response code.")
    public BoutiqueListingSteps requestToAllBoutiquePages() {

        List<String> hrefs = new ArrayList<>();
        int i = 0;

        Scroll.toFirstElement(getDriver(), boutiques);
        waitABit(5000);

        while (i < boutiques.size()) {
            try {
                String href = boutiques.get(i).getAttribute("href");
                hrefs.add(href);
            } catch (StaleElementReferenceException e) {
                Scroll.toLastElement(getDriver(), boutiques);
                String href = boutiques.get(i).getAttribute("href");
                hrefs.add(href);
            }
            i++;
        }

        writeBoutiqueUrlsToCsv(hrefs);

        return this;
    }

    private void writeBoutiqueUrlsToCsv(List<String> hrefs){

        List<String[]> links = new ArrayList<>();

        for(String href : hrefs){
            ValidatableResponse response = SimpleRequester.get(href);
            links.add(new String[]{href, String.valueOf(response.extract().statusCode())});
        }

        CsvUtils.writeDataLineByLine("src/test/resources/csv/boutiqueLinks.csv",
                new String[]{"Boutique Url,Response Code"}, links);
    }
}
