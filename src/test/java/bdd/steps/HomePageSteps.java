package bdd.steps;

import bdd.pages.HomePage;
import bdd.utils.javascript.JsCommon;
import bdd.utils.selenium.Action;
import bdd.utils.wait.PageWaits;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class HomePageSteps extends UIInteractionSteps {

    HomePage homePage;

    @Step("Open homepage.")
    public void openHomepage() {
        homePage.open();
        new PageWaits().waitForPageToCompleteState(getDriver());
        JsCommon.executeJs(getDriver(), "document.getElementsByClassName(" +
                "\"fancybox-item fancybox-stop\")[0].click()");
    }

    @Step("Open login popup.")
    public void openLoginPopup() {
        new PageWaits().waitForPageToCompleteState(getDriver());
        Action.hoverElement(getDriver(), HomePage.ACCOUNT_BUTTON);
        $(HomePage.ACCOUNT_LOGIN_BUTTON).click();
    }

    @Step("Type email as {0}.")
    public HomePageSteps typeEmail(String email) {

        $(HomePage.EMAIL_FIELD).typeAndTab(email);
        return this;
    }

    @Step("Type password as {0}.")
    public HomePageSteps typePassword(String password) {

        $(HomePage.PASSWORD_FIELD).typeAndTab(password);
        return this;
    }

    @Step("Submit login form.")
    public void submitLoginForm() {
        $(HomePage.LOGIN_BUTTON).click();
    }

    @Step("Is user logged in ?")
    public void isLoggedIn() {
        new PageWaits().waitForPageToCompleteState(getDriver());
        $(HomePage.MY_ACCOUNT_BUTTON).shouldBeVisible();
    }

    @Step("User should see error message as {0}")
    public void userShouldSeeErrorMessage(String errorText) {
        Assert.assertEquals(
                $(HomePage.ERROR_BOX).getText().trim(), errorText
        );
    }
}
