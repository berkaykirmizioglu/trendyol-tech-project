package bdd.utils.javascript;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Scroll {
    public static String toLastElement(WebDriver driver, List<WebElement> webElementList) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        int y = webElementList.get(webElementList.size() - 1).getLocation().getY();
        return (String) js.executeScript("var start = performance.now(); " +
                            "window.scrollTo(0," + y + "); " +
                            "var end = performance.now(); " +
                            "return '' + (end - start) + '';");
    }

    public static void toFirstElement(WebDriver driver, List<WebElement> webElementList) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        int y = webElementList.get(1).getLocation().getY();
        js.executeScript("window.scrollTo(0," + y + ")");
    }

    public static void scrollToElement(WebDriver driver, WebElement webElement) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        int y = webElement.getLocation().getY();
        js.executeScript("window.scrollTo(0," + y + ")");
    }
}
