package bdd.utils.javascript;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class JsCommon {
    public static String executeJs(WebDriver webDriver, String command) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver ;
        js.executeScript(command);
        return command;
    }
}
