package bdd.utils.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Action {

    public static void hoverElement(WebDriver driver, By by){
        Actions action = new Actions(driver);
        WebElement webElement = driver.findElement(by);
        action.moveToElement(webElement).build().perform();
    }
}
