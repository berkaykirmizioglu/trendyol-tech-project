package bdd.utils.csv;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CsvUtils {
    public static void writeDataLineByLine(String filePath, String [] header, List<String[]> data) {

        File file = new File(filePath);
        try {
            FileWriter outputfile = new FileWriter(file);

            Writer writer = Files.newBufferedWriter(Paths.get(filePath));

            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            csvWriter.writeNext(header);

            csvWriter.writeAll(data);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
