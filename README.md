## Running in Docker Containers ##

Compose up your grid;
````
cd docker/
docker-compose pull && docker-compose up
````

For scaling;
````
docker-compose scale chrome=5 firefox=5
````
## How to run tests ?


1. First of all, clone repository and follow
the **README.md** commands.
2. Go into project directoy (trendyol-tech-project)
3. In terminal, just type
````
mvn clean verify -Dwebdriver.driver=remote -Dwebdriver.remote.url="http://localhost:4444/wd/hub" -Dwebdriver.remote.driver=chrome
````
OR
````
mvn clean verify -Dwebdriver.driver=remote -Dwebdriver.remote.url="http://localhost:4444/wd/hub" -Dwebdriver.remote.driver=firefox
````

Note: if you want to run specific tags;

**Login Tag Example**
````
mvn clean verify -Dtags=login -Dwebdriver.driver=remote -Dwebdriver.remote.url="http://localhost:4444/wd/hub" -Dwebdriver.remote.driver=chrome
````

**Headless Mode**
````
mvn clean verify -Dtags=login -Dwebdriver.driver=remote -Dwebdriver.remote.url="http://localhost:4444/wd/hub" -Dwebdriver.remote.driver=chrome -D
````

## How to control test results ?

1. Go to '_target/site/serenity_' directory.
2. Open the **index.html** file


## Notes
1. boutiqueLinks.csv file is in the 'src/test/resources/csv' directory
2. scrollPerformance.csv file is in the 'src/test/resources/csv' directory




